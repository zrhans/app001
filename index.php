<?php
header('Content-Type: text/html; charset=utf-8'); //Esten header garante formatação utf-8

/**
* 	APLICAÇÃO
*
***/

// Definindo a variável que mostrará a página na section content
// no arquivo admin na view

// ajusta pagina de inicio caso o get seja vazio

/*

$include = (empty($_GET['pagina'])) ? 'listar' : $_GET['pagina'];

switch($include) {
	case 'criar':
		$include = 'pages-create.php';
		break;
	case 'editar':
		$include = 'pages-update.php';
		break;	
	case 'deletar':
		$include = 'pages-delete.php';
		break;
	case 'listar':
		$include = 'pages-index.php';
		break;
	default:
		$include = '404.php';
}
*/

define('DS', DIRECTORY_SEPARATOR); // dirname() retorna o caminho completo da pasta
define('ROOT', dirname(__FILE__)); // retorna o path do arquivo

include_once ROOT . DS . 'config/Config.php';
include_once ROOT . DS . 'src/Pages.php';

$pages = new Pages(new Config()); // Módulo (Classe) Pages precisa de uma instância da Classe config

$slug = (empty($_GET['pagina'])) ? 'pagina-inicial' : $_GET['pagina'];

$pagina = $pages->read($slug); // apenas uma
$paginas = $pages->read(); // todas

if (empty($pagina)) {
	header('HTTP/1.0 404 Not Found');
	$pagina['title'] = 'Página não encontrada';
	$pagina['body'] = '<h1>Páginam não encontrada</h1>';
}

/*
include ROOT.DS.'src'.DS.'Pages'.DS.$include;
*/
include ROOT.DS.'view'.DS.'front.php';
