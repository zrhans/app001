<?php
header('Content-Type: text/html; charset=utf-8'); //Esten header garante formatação utf-8

// Definindo a variável que mostrará a página na section content
// no arquivo admin na view

// ajusta pagina de inicio caso o get seja vazio

$include = (empty($_GET['pagina'])) ? 'listar' : $_GET['pagina'];

switch($include) {
	case 'criar':
		$include = 'pages-create.php';
		break;
	case 'editar':
		$include = 'pages-update.php';
		break;	
	case 'deletar':
		$include = 'pages-delete.php';
		break;
	case 'listar':
		$include = 'pages-index.php';
		break;
	default:
		$include = '404.php';
}
	
define('DS', DIRECTORY_SEPARATOR); // dirname() retorna o caminho completo da pasta
define('ROOT', dirname(__DIR__));

include_once ROOT . DS . 'config/Config.php';
include_once ROOT . DS . 'src/Pages.php';
include_once ROOT . DS . 'src/Users.php';

$pages = new Pages(new Config()); // Módulo (Classe) Pages precisa de uma instância da Classe config
$users = new Users(new Config()); // Módulo (Classe) Users precisa de uma instância da Classe config

$users->protege();

include ROOT.DS.'src'.DS.'Pages'.DS.$include;
include ROOT.DS.'view'.DS.'admin.php';