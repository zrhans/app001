<?php
header('Content-Type: text/html; charset=utf-8'); //Esten header garante formatação utf-8

define('DS', DIRECTORY_SEPARATOR); // dirname() retorna o caminho completo da pasta
define('ROOT', dirname(__DIR__));


include_once ROOT . DS . 'config/Config.php';
include_once ROOT . DS . 'src/Users.php';

$users = new Users(new Config()); // Módulo (Classe) Pages precisa de uma instância da Classe config

$users->logout();