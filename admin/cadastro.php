<?php
header('Content-Type: text/html; charset=utf-8'); //Esten header garante formatação utf-8

define('DS', DIRECTORY_SEPARATOR); // dirname() retorna o caminho completo da pasta
define('ROOT', dirname(__DIR__));


include_once ROOT . DS . 'config/Config.php';
include_once ROOT . DS . 'src/Users.php';

$users = new Users(new Config()); // Módulo (Classe) Users precisa de uma instância da Classe config
$users->protege();// somente se estiver logado pode cadastrar
$users->cadastrar($_POST);

?>
<!DOCTYPE html>
<html lang="pt_BR">
<head>
	<meta charset="UTF-8">
	<title>Admistração | Cadastro de Usuário</title>
	<link rel="stylesheet" href="../view/assets/css/admin.css">
</head>
<body>
	<header id="admheader">
		<center><img src="../view/assets/images/logo1.png" alt="Logo"><em>Admistração  <small>| Cadastro de Usuário</small></em><hr></center>
	</header>
	<menu>
		<ul>
			<li><a href="#">item de menu</a></li>
		</ul>
	</menu>
	<hr>
	<main>
		<section class="content">
		 <form action="cadastro.php" method="post">
		 	Nome:<br> <input type="text" name="nome"> <br> 
		 	Email:<br> <input type="email" name="email"> <br> 
		 	Usuário:<br> <input type="text" name="usuario"> <br> 
		 	Senha:<br> <input type="password" name="senha"> <br> 
		 	<br><input type="submit" value="Cadastrar">
		 </form>
		</section>
	</main>
</body>
</html>