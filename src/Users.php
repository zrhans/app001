<?php
	
class Users {
	public $db;
	
	function __construct(Config $config){
		$this->db = $config->conecta();		
	}
	
	function login($dados=null) {
		session_start();
		
		if ($_SERVER['REQUEST_METHOD']=='POST'){
			$usuario = $this->getUsuario($dados['usuario']);
			
			if ( password_verify($dados['senha'], $usuario['senha']) ) {
				$_SESSION['usuario'] = $usuario;				
				$this->lembrar($dados);
				header('Location: index.php');
			}
		} elseif ( !empty($_COOKIE['podesernome_da_app_aduia']) and !empty($_COOKIE['podesernome_da_app_oijfl']) ) {
			//Desencriptando o kookie
			$cookie['usuario'] = base64_decode($_COOKIE['podesernome_da_app_aduia']);
			$cookie['senha']  =  base64_decode($_COOKIE['podesernome_da_app_oijfl']);
			//prossegue a autenticação
			$usuario = $this->getUsuario($cookie['usuario']);
			if ( password_verify($cookie['senha'], $usuario['senha']) ) {	
				$_SESSION['usuario'] = $usuario;
				header('Location: index.php');
			}
		} 
		
	}
	
	function logout() {
		session_start();
		session_unset();
		session_destroy();
		
		//Destruindo os cookies na marra
		setcookie('podesernome_da_app_aduia');
		setcookie('podesernome_da_app_oijfl');
		
		header('Location: index.php');
	}
	
	function protege() {
		session_start();
		if (empty($_SESSION['usuario'])) {
			header('Location: login.php');
		}
	}
	
	function cadastrar($dados) {
		//Inserir os dados de Usuario
		if ($_SERVER['REQUEST_METHOD']=='POST') {
			
			$sql= 'INSERT INTO users (nome, email, usuario, senha) VALUES (:nome, :email, :usuario, :senha);';
			$cadastra = $this->db->prepare($sql);
			$cadastra->bindValue(':nome',$dados['nome'], PDO::PARAM_STR);
			$cadastra->bindValue(':email', $dados['email'], PDO::PARAM_STR);
			$cadastra->bindValue(':usuario', $dados['usuario'], PDO::PARAM_STR);
			$cadastra->bindValue(':senha', $this->hash($dados['senha']), PDO::PARAM_STR);
			$cadastra->execute();
			header('Location: login.php');
		}
		
	}
	
	function lembrar($dados) {
		$cookie = array(
			'usuario'=>base64_encode($dados['usuario']),
			'senha'=>base64_encode($dados['senha'])
		);
		                                                                      
		setcookie('podesernome_da_app_aduia', $cookie['usuario'], (time() + (360*24*3600)), $_SERVER['SERVER_NAME']);
		setcookie('podesernome_da_app_oijfl', $cookie['senha'], (time() + (360*24*3600)), $_SERVER['SERVER_NAME']);
		
	}
	
	function hash($senha) {
		return password_hash($senha, PASSWORD_BCRYPT, array('cost'=>12));
	}
	
	function getUsuario($user) {
		$sql= 'SELECT * FROM users WHERE usuario = :usuario;';
		$usuario = $this->db->prepare($sql);
		$usuario->bindValue(':usuario', $user, PDO::PARAM_STR);
		$usuario->execute();
		return $usuario->fetch();
	}
	
}