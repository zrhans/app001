<?php

if (empty($_GET['id'])){
	echo '<h1>Não permitido</h1>';
	exit;
} 

if ($_SERVER['REQUEST_METHOD'] == 'POST'){
	if ( !empty($_POST['title']) and !empty($_POST['body']) ) {
		
		$dados['title'] = $_POST['title'];
		$dados['body'] = $_POST['body'];	
		$pages->update($dados, $_POST['id']);
		$pagina = $pages->read(null, $_POST['id']); // Não esquecer o null para o slug
		header('Location:?id='.$pagina['id']);
	} else {
		echo "Falha no módulo Update<hr>";
		exit;
	}
} 

if (!empty($_GET['id'])) $pagina = $pages->read(null, $_GET['id']); 