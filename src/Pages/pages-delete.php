<?php
if (empty($_GET['id'])){
	echo '<h1>Não permitido</h1>';
	exit;
} 

if ($_SERVER['REQUEST_METHOD'] == 'POST') {	
	$pages->delete($_GET['id']);
	header('Location: index.php');
}

if (!empty($_GET['id'])) $pagina = $pages->read(null, $_GET['id']);  
