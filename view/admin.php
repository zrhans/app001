<?php
if (!isset($include) or !is_string($include)) {
	header('HTTP/1.0 500 Internal Server Error');
	die('<em>Variável include não encontrada.</em>');
}
?>
<!DOCTYPE html>
<html lang="pt_BR">
<head>
	<meta charset="UTF-8">
	<title>Admistração</title>
	<link rel="stylesheet" href="../view/assets/css/admin.css">
</head>
<body>
	<header id="admheader">
		<center><img src="../view/assets/images/logo1.png" alt="Logo"><em>Administração</em><hr></center>
	</header>
	<menu>
		<ul>
			<li><a href="index.php?pagina=criar">Nova Página</a></li>
			<li><a href="cadastro.php">Novo usuário</a></li>
			<li><a href="logout.php">Sair</a></li>
		</ul>
	</menu>
	<hr>
	<main>
		<section class="content">
		<?php include ROOT.DS.'view'.DS.'admin'.DS.$include;?>
		</section>
	</main>
</body>
</html>