<!DOCTYPE html>

<html lang="pt_BR">
<head>
	<meta charset="utf-8">
	<title>CRUD-PAGES | Inicio</title>	
</head>
<body>
 
	<h1>
		PAGES <small><em> pages-index</em></small>
	</h1>
	<table border='0'>
		<thead>
			<tr>
				<th>#ID</th>
				<th>TITLE</th>
				<th>SLUG</th>
				<th>AÇÕES</th>
			</tr>
		</thead>
		<tbody>
			<?php foreach($paginas as $pagina): ;?>
			<tr>
				<td><?php echo $pagina['id']; ?></td>
				<td><?php echo $pagina['title']; ?></td>
				<td><em><?php echo $pagina['slug']; ?></em></td>
				<td>
				<a href="index.php?pagina=editar&id=<?php echo $pagina['id']; ?>">Editar</a>
				| <a href="index.php?pagina=deletar&id=<?php echo $pagina['id']; ?>">Remover</a>
				</td>
			</tr>
			<?php endforeach; ?>
		</tbody>
	</table>
	
	<hr>
	 <a href="index.php?pagina=criar">Adicionar Nova</a> 
</body>
</html>	 
	

