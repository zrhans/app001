<!DOCTYPE html>
<html lang="pt_BR">
<head>
	<meta charset="UTF-8">
	<title><?php echo $pagina['title'] ?></title>
	<link rel="stylesheet" href="assets/css/style.css">
</head>
<body>
	<header>
		<img src="assets/images/logo1.png" alt="Logo">
		<em><?php echo $pagina['title']; ?></em>
		<hr>
	</header>
	<menu>
		<ul>
			<?php foreach($paginas as $menu) : ?>
			<li><a href="index.php?pagina=<?php echo $menu['slug'];?>"><?php echo $menu['title'];?></a></li>
			<?php endforeach; ?>
		</ul>
	</menu>
	
	<main>
		<section class="content">
		<?php echo $pagina['body']; ?>
		</section>
	</main>
</body>
</html>