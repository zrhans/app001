<?php

if (empty($_GET['id'])){
	echo '<h1>Não permitido</h1>';
	exit;
} 

include '../config/Config.php';
include '../src/Pages.php';

$pages = new Pages(new Config());

if ($_SERVER['REQUEST_METHOD'] == 'POST'){
	if ( !empty($_POST['title']) and !empty($_POST['body']) ) {
		
		$dados['title'] = $_POST['title'];
		$dados['body'] = $_POST['body'];	
		$pages->update($dados, $_POST['id']);
		$pagina = $pages->read(null, $_POST['id']); // Não esquecer o null para o slug
		header('Location:?id='.$pagina['id']);
	} else {
		echo "Falha no módulo Update<hr>";
		exit;
	}
} 

if (!empty($_GET['id'])) $pagina = $pages->read(null, $_GET['id']);  

?>

<!DOCTYPE html>

<html lang="pt_BR">
<head>
	<meta charset="utf-8">
	<title>CRUD-PAGES | Editar</title>	
</head>
<body>
	<em> <a href="index.php">Lições</a></em>
	<hr>
	
	
	<form name="form_pages" action="pages-update.php?id=<?php echo $pagina['id'];?>" method="POST">
		
		<input type="text" name="title" value="<?php echo $pagina['title'];?>">
		
		<label><br><em>Slug: <?php echo $pagina['slug'];?></em></label><br>
		
		<textarea name="body" id="" cols="80" rows="10"><?php echo $pagina['body'];?></textarea><br>
		<input type="hidden" name="id" value="<?php echo $pagina['id'];?>">
		<label>ID:<?php echo $pagina['id'];?></label><br>
		<input type="submit" value="Salvar">
	</form>
	
		<hr>
	 <a href="pages-index.php">CRUD-PAGES</a> 
	<hr>
	<h6>
		Resultado:
	</h6>
	<?php 
		echo '<b>Title: </b>'. $pagina['title'] . '<br>';
		echo '<b>Slug: </b><em>'. $pagina['slug'] . '</em><br>';
		echo '<b>Body: </b>'. $pagina['body'] . '<br>';	
	?>
</body>
</html>