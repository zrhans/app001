<?php
header('Content-Type: text/html; charset=utf-8');
include '../config/Config.php';
include '../src/Pages.php';

$pages = new Pages(new Config());
$paginas = $pages->read();
?> 

<!DOCTYPE html>

<html lang="pt_BR">
<head>
	<meta charset="utf-8">
	<title>CRUD-PAGES | Inicio</title>	
</head>
<body>
	<em> <a href="index.php">Lições</a></em>
	<hr>
	<h1>
		CRUD-PAGES <small><em> pages-index</em></small>
	</h1>
	<table border='1'>
		<thead>
			<tr>
				<th>#ID</th>
				<th>TITLE</th>
				<th>AÇÕES</th>
			</tr>
		</thead>
		<tbody>
			<?php foreach($paginas as $pagina): ;?>
			<tr>
				<td><?php echo $pagina['id']; ?></td>
				<td><?php echo $pagina['title']; ?></td>
				<td>
				<a href="pages-update.php?id=<?php echo $pagina['id']; ?>">Editar</a>
				| <a href="pages-delete.php?id=<?php echo $pagina['id']; ?>">Remover</a>
				</td>
			</tr>
			<?php endforeach; ?>
		</tbody>
	</table>
	
	<hr>
	 <a href="pages-create.php">Adicionar Novo</a> 
</body>
</html>	 
	

