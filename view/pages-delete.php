<?php

if (empty($_GET['id'])){
	echo '<h1>Não permitido</h1>';
	exit;
} 

include '../config/Config.php';
include '../src/Pages.php';

$pages = new Pages(new Config());

// Comente o porque verifica com POST e executa com GET:
// Porque para deletar o comando get deve vir do POST do formulário
// e nao diretamente da url (senão bastaria por o id na url para remover o registro)

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
	
	$pages->delete($_GET['id']);
		//$pagina = readId($db,$_POST['id']);
	header('Location: pages-index.php');
	}

if (!empty($_GET['id'])) $pagina = $pages->read(null, $_GET['id']);  

?>

<!DOCTYPE html>

<html lang="pt_BR">
<head>
	<meta charset="utf-8">
	<title>CRUD-PAGES | Delete</title>	
</head>
	
	<form name="form_pages" action="pages-delete.php?id=<?php echo $_GET['id'];?>" method="POST">
		<legend>
			Remover o registro #<?php echo $_GET['id'];?>
		</legend>
		<br>
		<input type="submit" value="Sim">
		<a href="pages-index.php">Não</a>
		<hr>		
		<input type="text" name="title" value="<?php echo $pagina['title'];?>">
		
		<label><br><em>Slug: <?php echo $pagina['slug'];?></em></label><br>
		
		<textarea name="body" id="" cols="80" rows="10"><?php echo $pagina['body'];?></textarea><br>
		<input type="hidden" name="id" value=<?php echo $pagina['id'];?>>
		<label>ID:<?php echo $pagina['id'];?></label><br>		
	</form>
 
	
		<hr>
	 <a href="pages-index.php">CRUD-PAGES</a> 
		<h6>
		Resultado:
	</h6>
	<?php 
		echo '<b>Title: </b>'. $pagina['title'] . '<br>';
		echo '<b>Slug: </b><em>'. $pagina['slug'] . '</em><br>';
		echo '<b>Body: </b>'. $pagina['body'] . '<br>';	
	?>
</body>
</html>