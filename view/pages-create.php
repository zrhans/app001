<?php

if ($_SERVER['REQUEST_METHOD'] == 'POST'){
	if ( !empty($_POST['title']) and !empty($_POST['body']) ) {
		
		include '../config/Config.php';
		include '../src/Pages.php';
		
		$pages = new Pages(new Config());
		
		$dados['title'] = $_POST['title'];
		$dados['body'] = $_POST['body'];	
		$pages->create($dados);
		header('Location: pages-index.php');
		exit;
		//var_dump($dados);
	}	
} 
?>

<!DOCTYPE html>

<html lang="pt_BR">
<head>
	<meta charset="utf-8">
	<title>CRUD-PAGES | Criar</title>	
</head>
<body>
	<em> <a href="index.php">Lições</a></em>
	<hr>
	
	<form name="form_pages" action="" method="POST">
		
		<input type="text" name="title"><br>
		<textarea name="body" id="" cols="80" rows="10"></textarea><br>
		
		<input type="submit" value="Enviar">
	</form>
	
	<hr>
	 <a href="pages-index.php">CRUD-PAGES</a> 	
</body>
</html>